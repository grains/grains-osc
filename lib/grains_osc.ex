defmodule GrainsOsc do
  @moduledoc """
  Documentation for GrainsOsc.
  """

  def create(id, ip, port) do
    Supervisor.start_child(GrainsOsc.Supervisor, [id, ip, port])
  end

  def send(id, channel, message) do
    GrainsOsc.Sender.send(id, channel, message)
  end

end
