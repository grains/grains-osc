defmodule GrainsOsc.Sender do


  @doc """
  starts a new sending process for OSC on `ip` (tuple) and `port`
  """
  def start_link(id, ip, port) do
    Agent.start_link(fn -> {ip, port} end, name: id)
  end

  @doc """
  Sends an OSC `message` with `value` on a given `channel`
  """

  def send(id, channel, message) do
    Agent.get(id, fn {ip, port} ->
      IO.puts "sending to #{channel}"
      ExOsc.Sender.send_message(ip, port, {channel, message})
      {ip, port}
    end)
  end


end
